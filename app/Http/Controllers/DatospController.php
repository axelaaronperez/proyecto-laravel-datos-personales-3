<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\datopersonal;
use Illuminate \ Support \ Facades \ Auth;

class DatospController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datop = datopersonal::all();
        return view('DatosPersonales.index',['datop'=>$datop]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('DatosPersonales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datop = new datopersonal();
        $datop->nombre = $request->nombre;
        $datop->apellidop = $request->apellidop;
        $datop->apellidom = $request->apellidom;
        $datop->fechanacimiento = $request->fechanacimiento;
        $datop->user_id = Auth::user()->id;

        if ($datop->save()) {
            return redirect("/DatosPersonales");
        }
        else {
            return view('DatosPersonales.create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
